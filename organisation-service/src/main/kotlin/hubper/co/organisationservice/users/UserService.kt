package hubper.co.organisationservice.users

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventhandling.gateway.EventGateway
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Service
import users.CreateUser
import users.GetAllUsers
import users.UserCreatedEvent
import users.User

@Service
class UserService (private val repository: UserRepository,
                   private val eventGateway: EventGateway) {

    @CommandHandler
    fun handle(cmd: CreateUser) {
        cmd.apply {
            repository.save(UserEntity(id, name))
            eventGateway.publish(UserCreatedEvent(id, name))
        }
    }

    @QueryHandler
    fun handle(query: GetAllUsers) = repository.findAll().map { it.toResponse() }.toTypedArray()

    private fun UserEntity.toResponse() = User(id, name)

}
