package hubper.co.organisationservice.users

import org.springframework.data.annotation.Id
import java.util.*

class UserEntity (@Id val id: UUID, val name: String)