package hubper.co.organisationservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OrganisationServiceApplication

fun main(args: Array<String>) {
    runApplication<OrganisationServiceApplication>(*args)
}
