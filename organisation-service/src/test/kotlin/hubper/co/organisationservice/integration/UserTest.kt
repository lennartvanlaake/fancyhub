package hubper.co.organisationservice.integration

import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.queryhandling.QueryGateway
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import users.createUser
import users.getAllUsers
import java.util.*

@SpringBootTest
class UserTest {

    @Autowired
    private lateinit var queryGateway: QueryGateway

    @Autowired
    private lateinit var commandGateway: CommandGateway

    @Test
    fun createAndGetUser() {
        val id = UUID.randomUUID()
        val name = "bla"
        commandGateway.createUser(id, name)
        val users = queryGateway.getAllUsers()
        check(users.isNotEmpty())
    }

}
