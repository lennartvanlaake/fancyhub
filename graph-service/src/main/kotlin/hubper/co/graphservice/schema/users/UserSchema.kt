package hubper.co.graphservice.schema.users

import users.User
import java.util.*

class UserResponse(val id: String, val name: String)

fun User.toResponse() = UserResponse(id.toString(), name)