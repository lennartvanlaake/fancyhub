package hubper.co.graphservice.schema.users

import com.expediagroup.graphql.annotations.GraphQLDescription
import com.expediagroup.graphql.spring.operations.Query
import extensions.blockingQueryForSingle
import org.axonframework.queryhandling.QueryGateway
import org.springframework.stereotype.Component
import users.GetAllUsers
import users.getAllUsers

@Component
class UserQueries(private val queryGateway: QueryGateway): Query {

    @GraphQLDescription("Get all users")
    fun getAllUsers(): List<UserResponse> =
            queryGateway.getAllUsers().map { it.toResponse() }

}
