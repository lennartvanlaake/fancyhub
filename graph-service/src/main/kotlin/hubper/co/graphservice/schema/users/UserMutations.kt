package hubper.co.graphservice.schema.users

import com.expediagroup.graphql.annotations.GraphQLDescription
import com.expediagroup.graphql.spring.operations.Mutation
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.stereotype.Component
import users.createUser
import java.util.*

@Component
class UserMutations(private val commandGateway: CommandGateway): Mutation {

    @GraphQLDescription("Create user")
    fun createUser(name: String): UserResponse {
        val id = UUID.randomUUID()
        commandGateway.createUser(id, name)
        return UserResponse(id.toString(), name)
    }

}
