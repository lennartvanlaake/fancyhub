package extensions

import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import java.util.concurrent.CompletableFuture

inline fun <reified R, reified Q> QueryGateway.queryForSingle(query: Q): CompletableFuture<R> {
    return this.query(query, ResponseTypes.instanceOf(R::class.java))
}

inline fun <reified R, reified Q> QueryGateway.blockingQueryForSingle(query: Q): R {
    return this.query(query, ResponseTypes.instanceOf(R::class.java)).get()
}
