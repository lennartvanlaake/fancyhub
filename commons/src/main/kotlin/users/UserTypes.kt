package users

import java.util.*

class User(val id: UUID, val name: String)