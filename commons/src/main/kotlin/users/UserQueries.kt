package users

import extensions.blockingQueryForSingle
import org.axonframework.queryhandling.QueryGateway

class GetAllUsers()

fun QueryGateway.getAllUsers(): Array<User> = blockingQueryForSingle(GetAllUsers())