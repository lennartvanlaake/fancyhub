package users

import java.util.*

data class UserCreatedEvent(val id: UUID, val name: String)