package users

import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.util.*

data class CreateUser(@TargetAggregateIdentifier val id: UUID, val name: String)

fun CommandGateway.createUser(id: UUID, name: String) =
        sendAndWait<Unit>(CreateUser(id, name))
